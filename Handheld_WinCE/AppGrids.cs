﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using C1.Win.C1FlexGrid;
using C1.Win.C1FlexGrid.Util;
using Handheld_WinCE.Utilities;

namespace Handheld_WinCE
{
    public class AppGrids
    {
        public C1FlexGrid PaveGrid { get; private set; }
        public C1FlexGrid DipsGrid { get; private set; }

        public BindingSource PaveSrc { get; set; }
        public BindingSource DipSrc { get; set; }

        public int UsedRows { get; private set; }

        private CellStyle sch;
        private CellStyle slvl;

        public AppGrids()
        {
            DipsGrid = new C1FlexGrid();
            PaveGrid = new C1FlexGrid();
            for (int i = 0; i <= 1; ++i)
            {
                var grid = (i == 0) ? PaveGrid : DipsGrid;
                grid.Dock = DockStyle.Fill;
                grid.Rows.Fixed = 2;
                grid.Cols.Fixed = 1;
                grid.Cols.Count = 31;
                grid.Cols.DefaultSize = 45;
                grid.KeyActionEnter = KeyActionEnum.MoveAcross;
                grid.KeyActionTab = KeyActionEnum.MoveDown;
                grid.AllowSorting = AllowSortingEnum.None;
                grid.ScrollTrack = false;
                grid.DrawMode = DrawModeEnum.OwnerDraw;

                grid.Cols[0].Width = 35;
                grid.Cols[1].Width = 60;
                for (int j = 2; j < grid.Cols.Count; ++j)
                {
                    grid.Cols[j].Width = 45;
                }

                grid.OwnerDrawCell += Grid_OwnerDrawCell;
            }

            UsedRows = 0;

            PaveGrid.CellChanged += Grid_CellChanged;
            DipsGrid.CellChanged += Grid_CellChanged;
            PaveSrc = new BindingSource();
            DipSrc = new BindingSource();
        }

        private void Grid_OwnerDrawCell(object sender, OwnerDrawCellEventArgs e)
        {
            var grid = sender as C1FlexGrid;
            if (grid != null)
            {
                if (e.Row >= grid.Rows.Fixed && e.Col == 0)
                {
                    e.Text = e.Row == grid.Rows.Count - 1 ? "Avgs" : e.Row.ToString();
                }
            }
        }

        public AppGrids(DataGridTables tables)
            : this()
        {
            PaveSrc.DataSource = tables.Paving;        
            DipSrc.DataSource = tables.DipSheet;

            PaveGrid.DataSource = PaveSrc;
            DipsGrid.DataSource = DipSrc;

            sch = PaveGrid.Styles.Add("Chainages");
            sch.Font = new Font(FontFamily.GenericSerif, 9, FontStyle.Bold);
            sch.TextAlign = TextAlignEnum.CenterCenter;

            DipsGrid.Styles.Add("Chainages", sch);
        }

        private void Grid_CellChanged(object sender, RowColEventArgs e)
        {
            var grid = sender as C1FlexGrid;
            if (grid != null && e.Col == 1)
            {
                sch = grid.Styles["Chainages"];
                grid.SetCellStyle(e.Row, e.Col, sch);
            }
        }

        public void PauseBinding()
        {
            PaveSrc.SuspendBinding();
            DipSrc.SuspendBinding();
        }

        public void ResumeBinding()
        {
            PaveSrc.ResumeBinding();
            DipSrc.ResumeBinding();
        }

        public void CopyDown(int row, int col, int tab)
        {
            var grid = tab.Equals(0) ? PaveGrid : DipsGrid;

            // col == 0 -> duplicate chainage step
            if (col == 1 && row > grid.Cols.Fixed)
            {
                var step = Math.Round(Chainage(row, tab) - Chainage(row - 1, tab), 2);
                grid[row + 1, col] = Math.Round(Chainage(row, tab) + step);
                grid.StartEditing(row + 1, col);
                grid.FinishEditing();               
            }
            // col > 0 -> copy down paving level
            else if (col > 1)
            {
                var last_row = CountUsedRows() - 1;
                var val = Cell(row, col, 0);
                if (!float.IsNaN(val))
                {
                    for (var i = row + 1; i <= last_row; ++i)
                    {
                        if (!PaveGrid[i, col].Equals(DBNull.Value))
                            break;
                        grid[i, col] = val;
                    }
                }
            }
        }

        internal int IsValidFile()
        {
            for (var i = 1; i <= CountUsedRows() - 1; ++i)
            {
                if ((float)PaveGrid[i + 1, 0] <= (float)PaveGrid[i, 0])
                {
                    return i + 1;
                }
            }
            return 0;
        }

        public int Cell(int row, int col, int tab)
        {
            return tab == 0
                ? (PaveGrid[row, col] is int ? (int)PaveGrid[row, col] : 0)
                : (DipsGrid[row, col] is int ? (int)DipsGrid[row, col] : 0);
        }

        public float Chainage(int row, int tab)
        {
            return tab == 0
                ? (PaveGrid[row, 1] is float ? (float)PaveGrid[row, 1] : float.NaN)
                : (DipsGrid[row, 1] is float ? (float)DipsGrid[row, 1] : float.NaN);
        }

        internal int CountUsedRows()
        {
            int rc = 1;
            float ch;

            do
            {
                ch = PaveGrid[rc, 1] is float ? (float)PaveGrid[rc++, 1] : float.NaN;
            } while (!float.IsNaN(ch) && ch >= 0);

            return rc - 1;
        }

        internal int CountUsedColumns()
        {
            var r = CountUsedRows();
            var c = 0;
            for (int i = 0; i < r; ++i)
            {
                while (!string.IsNullOrEmpty(PaveGrid[i, c] as string))
                {
                    ++c;
                }
            }

            return c;
        }

        public void Select(GridIndex idx, int tab)
        {
            Select(idx.Row, idx.Col, tab);
        }

        public void Select(int r, int c, int tab)
        {
            var grid = tab.Equals(0) ? PaveGrid : DipsGrid;

            grid.Select(r, c, true);
            grid.StartEditing(r, c);
        }
    }   
}
