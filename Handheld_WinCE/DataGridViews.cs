﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Handheld_WinCE
{
    public class DataGridViews
    {
        public DataGrid PavingGrid { get; private set; }
        public DataGrid DipGrid { get; private set; }

        public DataGridViews()
        {
            PavingGrid = new DataGrid();
            DipGrid = new DataGrid();
        }

        internal void CopyDown(int row, int col)
        {
            // col == 0 -> duplicate chainage step
            if (col == 0 && row > 0)
            {
                var step = Math.Round(CellVal(row, col, 0) - CellVal(row - 1, col, 0), 2);
                PavingGrid[row + 1, col] = Math.Round(CellVal(row, col, 0) + step).ToString();
            }
            // col > 0 -> copy down paving level
            else if (col > 0)
            {
                var last_row = CountUsedRows() - 1;
                var val = Cell(row, col, 0);
                if (!string.IsNullOrEmpty(val))
                {
                    for (var i = row + 1; i <= last_row; ++i)
                    {
                        if (!string.IsNullOrEmpty(PavingGrid[i, col] as string))
                            break;
                        PavingGrid[i, col] = val;
                    }
                }
            }
        }

        internal int IsValidFile()
        {
            for (var i = 0; i < CountUsedRows() - 1; ++i)
            {
                if (double.Parse(PavingGrid[i + 1, 0] as string) < double.Parse(PavingGrid[i, 0] as string))
                {
                    return i + 1;
                }
            }
            return 0;
        }

        internal string Cell(GridIndex idx, int tab)
        {
            return tab == 0 ? PavingGrid[idx.Row, idx.Col] as string : DipGrid[idx.RowD, idx.ColD] as string;
        }

        internal string Cell(int r, int c, int tab)
        {
            return tab == 0 ? PavingGrid[r, c] as string : DipGrid[r, c] as string;
        }

        internal double CellVal(GridIndex idx, int tab)
        {
            return tab == 0 ? CellVal(idx.Row, idx.Col, tab) : CellVal(idx.RowD, idx.ColD, tab);
        }

        internal double CellVal(int r, int c, int tab)
        {
            var dec = c == 0 ? 1 : 0;
            return tab == 0 ? Math.Round(double.Parse(PavingGrid[r, c] as string), dec)
                : Math.Round(double.Parse(DipGrid[r, c] as string), dec);
        }

        internal int CountUsedRows()
        {
            int crows = 0;
            var cont = true;
            while (cont)
            {
                var ch = PavingGrid[crows, 0] as string;
                cont = (!string.IsNullOrEmpty(ch) && double.Parse(ch) >= 0);
                ++crows;
            }
            return crows - 1;
        }

        internal int CountUsedColumns()
        {
            var r = CountUsedRows();
            var c = 0;
            for (int i = 0; i < r; ++i)
            {              
                while (!string.IsNullOrEmpty(PavingGrid[i, c] as string))
                {
                    ++c;
                }
            }

            return c;
        }
    }
}
