﻿using System;
using System.Windows.Forms;

namespace Handheld_WinCE
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.MenuFile = new System.Windows.Forms.MenuItem();
            this.MenuFile_SavePaving = new System.Windows.Forms.MenuItem();
            this.MenuFile_SaveAs_CSV = new System.Windows.Forms.MenuItem();
            this.MenuFile_SaveAs_PVS = new System.Windows.Forms.MenuItem();
            this.MenuFile_Open = new System.Windows.Forms.MenuItem();
            this.MenuFile_CheckFile = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.MenuEdit = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Cut = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Copy = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Paste = new System.Windows.Forms.MenuItem();
            this.MenuEdit_CpyDwn = new System.Windows.Forms.MenuItem();
            this.MenuEdit_MakeEmptyDipSheet = new MenuItem();
            this.MenuEdit_MakePavingDipSheet = new MenuItem();
            this.MenuEdit_MakeDipSheet = new System.Windows.Forms.MenuItem();
            this.DataGrids = new DataGridViews();
            this.MainTabCntrl = new System.Windows.Forms.TabControl();
            this.TabPavingData = new System.Windows.Forms.TabPage();
            this.TabDipSheet = new System.Windows.Forms.TabPage();
            this.MenuFile_SaveDipSheet = new System.Windows.Forms.MenuItem();
            this.MenuFile_SaveDipSheetCSV = new System.Windows.Forms.MenuItem();
            this.MenuFile_SaveDipSheetText = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.MainTabCntrl.SuspendLayout();
            this.TabPavingData.SuspendLayout();
            this.TabDipSheet.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.MenuFile);
            this.mainMenu1.MenuItems.Add(this.MenuEdit);
            // 
            // MenuFile
            // 
            this.MenuFile.MenuItems.Add(this.MenuFile_SavePaving);
            this.MenuFile.MenuItems.Add(this.MenuFile_SaveDipSheet);
            this.MenuFile.MenuItems.Add(this.MenuFile_Open);
            this.MenuFile.MenuItems.Add(this.menuItem7);
            this.MenuFile.MenuItems.Add(this.MenuFile_CheckFile);
            this.MenuFile.MenuItems.Add(this.menuItem2);
            this.MenuFile.MenuItems.Add(this.menuItem4);
            this.MenuFile.MenuItems.Add(this.menuItem5);
            this.MenuFile.Text = "FILE";
            // 
            // MenuFile_SavePaving
            // 
            this.MenuFile_SavePaving.MenuItems.Add(this.MenuFile_SaveAs_CSV);
            this.MenuFile_SavePaving.MenuItems.Add(this.MenuFile_SaveAs_PVS);
            this.MenuFile_SavePaving.Text = "Save Paving Lvls";
            // 
            // MenuFile_SaveAs_CSV
            // 
            this.MenuFile_SaveAs_CSV.Text = "CSV";
            this.MenuFile_SaveAs_CSV.Click += new System.EventHandler(this.MenuFile_SaveAs_CSV_Click);
            // 
            // MenuFile_SaveAs_PVS
            // 
            this.MenuFile_SaveAs_PVS.Text = "PVS";
            this.MenuFile_SaveAs_PVS.Click += new System.EventHandler(this.MenuFile_SaveAs_PVS_Click);
            // 
            // MenuFile_Open
            // 
            this.MenuFile_Open.Text = "Open";
            this.MenuFile_Open.Click += new System.EventHandler(this.MenuFile_Open_Click);
            // 
            // MenuFile_CheckFile
            // 
            this.MenuFile_CheckFile.Text = "Check File";
            this.MenuFile_CheckFile.Click += new System.EventHandler(this.MenuFile_CheckFile_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "Help";
            // 
            // menuItem5
            // 
            this.menuItem5.Text = "About...";
            // 
            // MenuEdit
            // 
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Cut);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Copy);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Paste);
            this.MenuEdit.MenuItems.Add(this.menuItem3);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_CpyDwn);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_MakeDipSheet);
            this.MenuEdit.Text = "EDIT";
            this.MenuEdit.Popup += new System.EventHandler(this.MenuEdit_Popup);
            // 
            // MenuEdit_Cut
            // 
            this.MenuEdit_Cut.Text = "Cut";
            this.MenuEdit_Cut.Click += new System.EventHandler(this.MenuEdit_Cut_Click);
            // 
            // MenuEdit_Copy
            // 
            this.MenuEdit_Copy.Text = "Copy";
            this.MenuEdit_Copy.Click += new System.EventHandler(this.MenuEdit_Copy_Click);
            // 
            // MenuEdit_Paste
            // 
            this.MenuEdit_Paste.Enabled = false;
            this.MenuEdit_Paste.Text = "Paste";
            this.MenuEdit_Paste.Click += new System.EventHandler(this.MenuEdit_Paste_Click);
            // 
            // MenuEdit_CpyDwn
            // 
            this.MenuEdit_CpyDwn.Text = "Copy Down [c]";
            this.MenuEdit_CpyDwn.Click += new System.EventHandler(this.MenuEdit_CpyDwn_Click);
            //
            // MenuEdit_MakeEmptyDipSheet
            //
            this.MenuEdit_MakeEmptyDipSheet.Text = "Empty";
            this.MenuEdit_MakeEmptyDipSheet.Click += MenuEdit_MakeEmptyDipSheet_Click;
            //
            //
            //
            this.MenuEdit_MakePavingDipSheet.Text = "From Paving Lvls";
            this.MenuEdit_MakePavingDipSheet.Click += MenuEdit_MakePavingDipSheet_Click;
            // 
            // MenuEdit_MakeDipSheet
            // 
            this.MenuEdit_MakeDipSheet.Text = "Make Dip-Sheet";
            this.MenuEdit_MakeDipSheet.MenuItems.Add(this.MenuEdit_MakeEmptyDipSheet);
            this.MenuEdit_MakeDipSheet.MenuItems.Add(this.MenuEdit_MakePavingDipSheet);
            // 
            // MainDataGrid
            // 
            this.DataGrids.PavingGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DataGrids.PavingGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrids.PavingGrid.Location = new System.Drawing.Point(0, 0);
            this.DataGrids.PavingGrid.Name = "MainDataGrid";
            this.DataGrids.PavingGrid.RowHeadersVisible = false;
            this.DataGrids.PavingGrid.Size = new System.Drawing.Size(240, 245);
            this.DataGrids.PavingGrid.TabIndex = 0;
            this.DataGrids.PavingGrid.CurrentCellChanged += new System.EventHandler(this.DataGrids_CurrentCellChanged);
            this.DataGrids.PavingGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGrid_KeyPress);
            // 
            // MainTabCntrl
            // 
            this.MainTabCntrl.Controls.Add(this.TabPavingData);
            this.MainTabCntrl.Controls.Add(this.TabDipSheet);
            this.MainTabCntrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabCntrl.Location = new System.Drawing.Point(0, 0);
            this.MainTabCntrl.Name = "MainTabCntrl";
            this.MainTabCntrl.SelectedIndex = 0;
            this.MainTabCntrl.Size = new System.Drawing.Size(240, 268);
            this.MainTabCntrl.TabIndex = 1;
            // 
            // TabPavingData
            // 
            this.TabPavingData.Controls.Add(this.DataGrids.PavingGrid);
            this.TabPavingData.Location = new System.Drawing.Point(0, 0);
            this.TabPavingData.Name = "TabPavingData";
            this.TabPavingData.Size = new System.Drawing.Size(240, 245);
            this.TabPavingData.Text = "Paving Lvls";
            // 
            // TabDipSheet
            // 
            this.TabDipSheet.Controls.Add(this.DataGrids.DipGrid);
            this.TabDipSheet.Location = new System.Drawing.Point(0, 0);
            this.TabDipSheet.Name = "TabDipSheet";
            this.TabDipSheet.Size = new System.Drawing.Size(240, 245);
            this.TabDipSheet.Text = "Dip Sheet";
            // 
            // DipDataGrid
            // 
            this.DataGrids.DipGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.DataGrids.DipGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrids.DipGrid.Location = new System.Drawing.Point(0, 0);
            this.DataGrids.DipGrid.Name = "DipDataGrid";
            this.DataGrids.DipGrid.RowHeadersVisible = false;
            this.DataGrids.DipGrid.Size = new System.Drawing.Size(240, 245);
            this.DataGrids.DipGrid.TabIndex = 1;
            this.DataGrids.DipGrid.CurrentCellChanged += new System.EventHandler(this.DataGrids_CurrentCellChanged);
            this.DataGrids.DipGrid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGrid_KeyPress);
            // 
            // MenuFile_SaveDipSheet
            // 
            this.MenuFile_SaveDipSheet.MenuItems.Add(this.MenuFile_SaveDipSheetCSV);
            this.MenuFile_SaveDipSheet.MenuItems.Add(this.MenuFile_SaveDipSheetText);
            this.MenuFile_SaveDipSheet.Text = "Save Dip-Sheet";
            // 
            // MenuFile_SaveDipSheetCSV
            // 
            this.MenuFile_SaveDipSheetCSV.Text = "CSV";
            this.MenuFile_SaveDipSheetCSV.Click += MenuFile_SaveDipSheetCSV_Click;
            // 
            // MenuFile_SaveDipSheetText
            // 
            this.MenuFile_SaveDipSheetText.Text = "Text";
            this.MenuFile_SaveDipSheetText.Click += MenuFile_SaveDipSheetText_Click;
            // 
            // menuItem7
            // 
            this.menuItem7.Text = "-";
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "-";
            // 
            // menuItem3
            // 
            this.menuItem3.Text = "-";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.MainTabCntrl);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = true; // set to false to have 'ok' button
            this.Name = "MainForm";
            this.Text = "PAVESET HANDHELD";
            this.MainTabCntrl.ResumeLayout(false);
            this.TabPavingData.ResumeLayout(false);
            this.TabDipSheet.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Handheld_WinCE.DataGridViews DataGrids;
        private System.Windows.Forms.MenuItem MenuFile;
        private System.Windows.Forms.MenuItem MenuFile_SavePaving;
        private System.Windows.Forms.MenuItem MenuEdit;
        private System.Windows.Forms.MenuItem MenuFile_Open;
        private System.Windows.Forms.MenuItem MenuFile_CheckFile;
        private System.Windows.Forms.MenuItem MenuFile_SaveAs_CSV;
        private System.Windows.Forms.MenuItem MenuFile_SaveAs_PVS;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem5;
        private System.Windows.Forms.MenuItem MenuEdit_Cut;
        private System.Windows.Forms.MenuItem MenuEdit_Copy;
        private System.Windows.Forms.MenuItem MenuEdit_Paste;
        private System.Windows.Forms.MenuItem MenuEdit_CpyDwn;
        private TabControl MainTabCntrl;
        private TabPage TabPavingData;
        private TabPage TabDipSheet;
        private MenuItem MenuEdit_MakeDipSheet;
        private MenuItem MenuEdit_MakePavingDipSheet;
        private MenuItem MenuEdit_MakeEmptyDipSheet;
        private MenuItem MenuFile_SaveDipSheet;
        private MenuItem MenuFile_SaveDipSheetCSV;
        private MenuItem MenuFile_SaveDipSheetText;
        private MenuItem menuItem7;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
    }
}

