﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.WindowsCE.Forms;

namespace Handheld_WinCE
{
    public partial class MainForm : Form
    {
        #region ---------- Members & Constructors/Initialisation --------------

        private int cut_row, cut_col;
        private readonly Thread background;

        private readonly Notification file_note = new Notification
        {
            Caption = "FILE I/O", InitialDuration = 4, Visible = false
        };

        public string PavesetPath { get { return "\\Storage Card\\Paveset"; } }
        public string JobName { get; private set; }

        public EditOpts EditOpt { get; private set; }
        public string PasteStr { get; private set; }
        public int PasteTab { get; private set; }

        private readonly GridIndex index = new GridIndex();

        public DataGridTables Tables { get; private set; }
        private BindingSource bind_src = new BindingSource();

        public int SelectedTab { get; private set; }

        /// <summary>
        /// TODO: Update Comments
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            Tables = new DataGridTables();
            bind_src.DataSource = Tables.Paving;
            DataGrids.PavingGrid.DataSource = bind_src;

            background = new Thread(BackgroundStart) { IsBackground = true };
            SelectedTab = 0;

            JobName = "Book";

            MainTabCntrl.SelectedIndexChanged += MainTabCntrl_SelectedIndexChanged;
            if (!Directory.Exists(PavesetPath))
            {
                Directory.CreateDirectory(PavesetPath);
            }

            EditOpt = EditOpts.Nothing;
            PasteStr = string.Empty;

            Load += (sender, args) => background.Start();
        }

        private void MainTabCntrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var tabs = sender as TabControl ?? MainTabCntrl;
            SelectedTab = tabs.SelectedIndex;
            Text = SelectedTab == 0 ? "PAVESET - PAVING LVLS" : "PAVESET - DIP-SHEET";
        }

        private void BackgroundStart()
        {
            for (int i = 0; i < 500; ++i)
            {
                Tables.AddRow();
            }
        } 

        #endregion

        #region ---------- Menu & Event Handlers ------------------------------

        private void DataGrids_CurrentCellChanged(object sender, EventArgs e)
        {
            if (SelectedTab == 0) { index.Update(DataGrids.PavingGrid.CurrentCell); }
            if (SelectedTab == 1) { index.UpdateDip(DataGrids.DipGrid.CurrentCell); }
        }

        private void DataGrid_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataGrid grid = SelectedTab == 0 ? DataGrids.PavingGrid : DataGrids.DipGrid;
            var r = SelectedTab == 0 ? index.Row : index.RowD;
            var c = SelectedTab == 0 ? index.Col : index.ColD;
            var str = DataGrids.Cell(index, SelectedTab);

            if (char.IsDigit(e.KeyChar))
            {
                grid[r, c] = str + e.KeyChar;
            }
            else if (char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                switch (e.KeyChar)
                {
                    case (char)0x08:
                        if (!string.IsNullOrEmpty(str))
                        {
                            grid[r, c] = str.Substring(0, str.Length - 1);
                        }
                        break;
                    case '.':
                        grid[r, c] = str + ".";
                        break;
                    case (char)Keys.Enter:
                        if (grid.CurrentCell.ColumnNumber != grid.VisibleColumnCount - 1)
                        {
                            
                        }
                        break;
                    default:
                        e.Handled = true;
                        break;
                }
            }
            else
            {
                switch (e.KeyChar)
                {
                    case 'c':
                    case 'C':
                        if (SelectedTab == 0) { DataGrids.CopyDown(index.Row, index.Col); }
                        break;
                }
                e.Handled = true;
            }
        }

        private void MenuFile_CheckFile_Click(object sender, EventArgs e)
        {
            // Check file 
            var note = new Notification()
            {
                Caption = "FILE CHECK",
                InitialDuration = 5,
                Critical = true,
                Visible = false
            };

            var res = DataGrids.IsValidFile(); // function that checks the data

            note.Text = res == 0 ? "<b>File is OK...<b>" : "<b>File Error At Row: " + res + "<b>";
            note.Visible = true;
        }

        private void MenuFile_SaveAs_CSV_Click(object sender, EventArgs e)
        {
            if (DataGrids.IsValidFile() == 0)
            {
                var dlg = new SaveFileDialog
                {
                    InitialDirectory = PavesetPath,
                    Filter = "CSV Files (*.csv)|*.csv",
                    FileName = JobName
                };

                var dlg_res = dlg.ShowDialog();

                if (dlg_res == DialogResult.OK)
                {
                    var res = DataFiles.SaveCSV(Tables.Paving, dlg.FileName);

                    file_note.Text = "<b>" + (res ? "File Saved Successfully: <p>" + dlg.FileName + "<p>" : "File Save Failed!") + "<b>";
                    file_note.Visible = true;
                }
            }
        }

        /// <summary>
        /// TODO: Finish Implementation
        /// </summary>
        private void MenuFile_SaveAs_PVS_Click(object sender, EventArgs e)
        {

        }

        private void MenuFile_Open_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog
            {
                Filter = "CSV Files (*.csv)|*.csv|PVS Files (*.pvs)|*.pvs",
                InitialDirectory = PavesetPath
            };

            var res = dlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                Tables.ClearRows();
                var rows = DataFiles.OpenCSV(dlg.FileName);
                Tables.FillTable(rows);

                file_note.Text = "<b>" + (rows != null && rows.Count > 0 ? "File Read In: <p>" + dlg.FileName + "<p>" : "File Read Error!") + "<b>";
                file_note.Visible = true;
            }
        }

        private void MenuEdit_Cut_Click(object sender, EventArgs e)
        {
            var grid = SelectedTab == 0 ? DataGrids.PavingGrid : DataGrids.DipGrid;
            PasteStr = DataGrids.Cell(index, SelectedTab);
            EditOpt = (string.IsNullOrEmpty(PasteStr) ? EditOpts.Nothing : EditOpts.Cut);

            if (EditOpt == EditOpts.Cut)
            {
                PasteTab = SelectedTab;
                cut_row = grid.CurrentCell.RowNumber;
                cut_col = grid.CurrentCell.ColumnNumber;
            }
        }

        private void MenuEdit_Copy_Click(object sender, EventArgs e)
        {
            PasteStr = DataGrids.Cell(index, SelectedTab);
            PasteTab = SelectedTab;
            EditOpt = (string.IsNullOrEmpty(PasteStr) ? EditOpts.Nothing : EditOpts.Copy);
        }

        private void MenuEdit_Paste_Click(object sender, EventArgs e)
        {
            var grid = SelectedTab == 0 ? DataGrids.PavingGrid : DataGrids.DipGrid;
            var r = SelectedTab == 0 ? index.Row : index.RowD;
            var c = SelectedTab == 0 ? index.Col : index.ColD;
            grid[r, c] = PasteStr;

            if (EditOpt == EditOpts.Cut)
            {
                grid[cut_row, cut_col] = string.Empty;
            }

            grid[r, c] = grid[r, c];
            EditOpt = EditOpts.Nothing;
            PasteStr = string.Empty;
        }

        private void MenuEdit_Popup(object sender, EventArgs e)
        {
            MenuEdit_Paste.Enabled = (EditOpt != EditOpts.Nothing && SelectedTab == PasteTab);
            MenuEdit_Cut.Enabled = (SelectedTab == 0);
            MenuEdit_Copy.Enabled = (SelectedTab == 0);
            MenuEdit_CpyDwn.Enabled = (SelectedTab == 0);
        }

        private void MenuEdit_CpyDwn_Click(object sender, EventArgs e) { DataGrids.CopyDown(index.Row, index.Col); }

        private void MenuEdit_MakePavingDipSheet_Click(object sender, EventArgs e)
        {
            DataGrids.DipGrid.DataSource = null;
            DataGrids.DipGrid.Refresh();

            Tables.CreateDipSheetTable(DataGrids.CountUsedRows(), DataGrids.CountUsedColumns());

            DataGrids.DipGrid.DataSource = Tables.DipSheet;

            MainTabCntrl.SelectedIndex = 1;
        }

        private void MenuEdit_MakeEmptyDipSheet_Click(object sender, EventArgs event_args)
        {
            DataGrids.DipGrid.DataSource = null;
            DataGrids.DipGrid.Refresh();

            Tables.CreateDipSheetTable(500, 2);

            DataGrids.DipGrid.DataSource = Tables.DipSheet;

            MainTabCntrl.SelectedIndex = 1;
        }

        private void MenuFile_SaveDipSheetText_Click(object sender, EventArgs event_args)
        {

        }

        private void MenuFile_SaveDipSheetCSV_Click(object sender, EventArgs event_args)
        {
            if (DataGrids.DipGrid != null)
            {
                var dlg = new SaveFileDialog
                {
                    InitialDirectory = PavesetPath,
                    Filter = "CSV Files (*.csv)|*.csv",
                    FileName = JobName + "_DipSheet"
                };

                var dlg_res = dlg.ShowDialog();

                if (dlg_res == DialogResult.OK)
                {
                    var res = DataFiles.SaveCSV(Tables.DipSheet, dlg.FileName);

                    file_note.Text = "<b>" + (res ? "File Saved Successfully: <p>" + dlg.FileName + "<p>" : "File Save Failed!") + "<b>";
                    file_note.Visible = true;
                }
            }
        }

        #endregion
    }

    #region ---------- Helper/Utility Classes & Enums -------------------------

    public enum EditOpts
    {
        Nothing, Copy, Cut
    }

    public class GridIndex
    {
        public int Row { get; private set; }
        public int Col { get; private set; }
        public int RowD { get; private set; }
        public int ColD { get; private set; }

        public GridIndex()
        {
            Row = 0;
            Col = 0;
            RowD = 0;
            ColD = 0;
        }

        public void Update(DataGridCell cell)
        {
            Update(cell.RowNumber, cell.ColumnNumber);
        }

        public void Update(int r, int c)
        {
            Row = r;
            Col = c;
        }

        public void UpdateDip(DataGridCell cell)
        {
            UpdateDip(cell.RowNumber, cell.ColumnNumber);
        }

        public void UpdateDip(int r, int c)
        {
            RowD = r;
            ColD = c;
        }
    } 

    #endregion
}