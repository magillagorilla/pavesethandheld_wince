﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Handheld_WinCE
{
    /// <summary>
    /// TODO: Update Comments
    /// </summary>
    public static class DataFiles
    {
        /// <summary>
        /// TODO: Update Comments
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="fp"></param>
        /// <returns></returns>
        public static bool SaveCSV(DataTable tbl, string fp)
        {
            try
            {
                using (var fs = new FileStream(fp, FileMode.Create))
                using (var writer = new StreamWriter(fs))
                {
                    foreach (DataRow row in tbl.Rows)
                    {
                        var line = string.Join(",", row.ItemArray.Select(x => x as string).ToArray());
                        writer.WriteLine(line);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// TODO: Update Comments & Finish Impplementation
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="fp"></param>
        /// <returns></returns>
        public static bool SavePVS(DataTable tbl, string fp)
        {
            try
            {
                using (var fs = new FileStream(fp, FileMode.Create))
                using (var writer = new StreamWriter(fs))
                {
                    foreach (DataRow row in tbl.Rows)
                    {
                        
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// TODO: Update Comments
        /// </summary>
        /// <param name="fp"></param>
        /// <returns></returns>
        public static List<List<string>> OpenFile(string fp)
        {
            var ext = fp.Substring(fp.LastIndexOf('.') + 1).ToUpper();
            return ext == "PVS" ? OpenPVS(fp) 
                : (ext == "CSV" ? OpenCSV(fp) 
                : null);
        }

        /// <summary>
        /// TODO: Update Comments
        /// </summary>
        /// <param name="fp"></param>
        /// <returns></returns>
        public static List<List<string>> OpenCSV(string fp)
        {
            var rows = new List<List<string>>();

            using (var reader = new StreamReader(fp))
            {
                do
                {
                    var line = reader.ReadLine().Split(',').ToList();
                    rows.Add(line);

                    if (line.Count < 31)
                    {
                        rows.Last().AddRange(Enumerable.Repeat("", 31 - line.Count));
                    }
                } while (!reader.EndOfStream);
            }

            return rows;
        }

        /// <summary>
        /// TODO: Update Comments & Finish Impplementation
        /// </summary>
        /// <param name="fp"></param>
        /// <returns></returns>
        public static List<List<string>> OpenPVS(string fp)
        {
            var rows = new List<List<string>>();

            using (var reader = new StreamReader(fp))
            {
            }

            return rows;
        }

        /// <summary>
        /// TODO: Update Comments
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="first_ch"></param>
        /// <param name="last_ch"></param>
        /// <returns></returns>
        internal static byte[] MakePvsHeader(int rows, int first_ch, int last_ch)
        {
            var header = new byte[64];

            return header;
        }
    }
}
