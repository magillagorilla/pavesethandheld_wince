﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;
using Handheld_WinCE.Utilities;
using Microsoft.WindowsCE.Forms;
using Handheld_WinCE.Utilities;

namespace Handheld_WinCE
{
    public partial class AppForm : Form
    {
        private readonly Thread background;

        public AppGrids Grids { get; private set; }
        public DataGridTables Tables { get; private set; }

        public string PavesetPath { get { return "\\Storage Card\\Paveset"; } }
        public string JobName { get; private set; }

        public GridIndex Index { get; private set; }

        public AppForm()
        {
            InitializeComponent();
            //background = new Thread(BackgroundStart) { IsBackground = true };

            Tables = new DataGridTables();
            Grids = new AppGrids(Tables);

            TabPaving.Controls.Add(Grids.PaveGrid);
            TabDipsheet.Controls.Add(Grids.DipsGrid);

            JobName = "Book";

            Load += AppForm_OnLoad;
            
            Grids.PaveGrid.SelChange += Grid_SelectedCellChanged;
            Grids.DipsGrid.SelChange += Grid_SelectedCellChanged;
            Index = new GridIndex();
        }

        private void Grid_SelectedCellChanged(object sender, EventArgs e)
        {
            var grid = sender as C1FlexGrid;
            if (grid != null)
            {
                Index.Update(grid.RowSel, grid.ColSel);
            }
        }

        private void AppForm_OnLoad(object sender, EventArgs e)
        {
            BeginInvoke(new InvokeDelegate(Init));
        }

        public void Init()
        {
            Tables.AddRows(25, 0);

            Grids.DipsGrid.DataSource = null;
            Grids.DipsGrid.Refresh();

            Tables.CreateDipSheetTable(25, 2, false);
            Grids.DipsGrid.DataSource = Tables.DipSheet;
            Grids.Select(2, 1, 0);
        }

        private void AppForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                switch ((Keys)e.KeyChar)
                {
                    case Keys.Enter:
                        break;
                    case Keys.Up:
                        break;
                    case Keys.Down:
                        break;
                    case Keys.Left:
                        break;
                    case Keys.Right:
                        break;
                }
            }
        }

        private void MenuEdit_DipEmpty_Click(object sender, EventArgs e)
        {
            Grids.DipsGrid.DataSource = null;
            Grids.DipsGrid.Refresh();

            Tables.CreateDipSheetTable(25, 2, false);

            Grids.DipsGrid.DataSource = Tables.DipSheet;

            AppTabs.SelectedIndex = 1;

            Grids.Select(2, 1, Index.Tab);
        }

        private void MenuEdit_DipPaving_Click(object sender, EventArgs e)
        {

        }

        private void MenuFile_Check_Click(object sender, EventArgs e)
        {
            // Check file 
            var note = new Notification()
            {
                Caption = "FILE CHECK",
                InitialDuration = 5,
                Visible = false
            };

            var res = Grids.IsValidFile(); // function that checks the data

            note.Text = res == 0 ? "<b>File is OK...<b>" : "<b>File Error At Row: " + res + "<b>";
            note.Visible = true;
        }

        private void MenuFile_SavePaving_Click(object sender, EventArgs e)
        {
            if (Grids.IsValidFile() == 0)
            {
                var dlg = new SaveFileDialog
                {
                    InitialDirectory = PavesetPath,
                    Filter = "CSV File (*.csv)|*.csv|PVS File (*.pvs)|*.pvs|Excel File (*.xls)|*.xls",
                    FileName = JobName
                };

                var dlg_res = dlg.ShowDialog();

                if (dlg_res == DialogResult.OK)
                {
                    var res = true;
                    switch (dlg.FilterIndex)
                    {
                        case 1: // CSV
                            res = DataFiles.SaveCSV(Tables.Paving, dlg.FileName);
                            break;
                        case 2: // PVS
                            res = DataFiles.SavePVS(Tables.Paving, dlg.FileName);
                            break;
                        case 3: // xls
                            break;
                    }

                    var note = new Notification
                    {
                        Caption = "FILE I/O",
                        InitialDuration = 4,
                        Visible = false,
                        Text =
                            "<b>" + (res ? "File Saved Successfully: <p>" + dlg.FileName + "<p>" : "File Save Failed!") +
                            "<b>"
                    };

                    note.Visible = true;
                }
            }
        }

        private void MenuFile_SaveDips_Click(object sender, EventArgs e)
        {
            if (Grids.DipsGrid != null)
            {
                
            }
        }

        private void AppForm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    break;
                case Keys.Up:
                    break;
                case Keys.Down:
                    if (Index.Tab == 0 && Index.Row >= Grids.PaveGrid.Rows.Count - 1)
                    {
                        Tables.AddRows(1, 0);
                    }
                    break;
                case Keys.Left:
                    break;
                case Keys.Right:
                    break;
                case (Keys)'c':
                case (Keys)'C':
                    DoCopyDown();
                    break;
            }
        }

        private void MenuEdit_CopyDown_Click(object sender, EventArgs e)
        {
            DoCopyDown();         
        }

        private void AppTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            Index.Tab = AppTabs.SelectedIndex;
            Grids.Select(Index.Row, Index.Col, Index.Tab);
        }

        internal void DoCopyDown()
        {
            var grid = Index.Tab.Equals(0) ? Grids.PaveGrid : Grids.DipsGrid;
            grid.FinishEditing();
            Grids.CopyDown(Index.Row, Index.Col, Index.Tab);

            if (Index.Row >= grid.Rows.Count - 1)
            {
                Tables.AddRows(1, Index.Tab);
            }
        }

        private void MenuEdit_Popup(object sender, EventArgs e)
        {
            MenuEdit_CopyDown.Enabled = Index.Tab != 1 || Index.Col == 0;
        }

        private void MenuEdit_CalcAvgs_Click(object sender, EventArgs e)
        {
            var grid = Index.Tab.Equals(0) ? Grids.PaveGrid : Grids.DipsGrid;
            var tbl = Index.Tab.Equals(0) ? Tables.Paving : Tables.DipSheet;

            var sum = Enumerable.Repeat(0.0, grid.Cols.Count).ToArray();
            for (int i = 0; i < tbl.Rows.Count; ++i)
            {
                var row = Tables.Paving.Rows[i].ItemArray.Skip(1).Cast<int>().ToArray();

                for (int j = 0; j < tbl.Columns.Count - 1; ++j)
                {
                    sum[j] += Convert.ToDouble(row[j]);
                }
            }
        }
    }

    public delegate void InvokeDelegate();
}

/*

Grids.PaveGrid.SubtotalPosition = SubtotalPositionEnum.BelowData;

            Grids.PaveGrid.Subtotal(AggregateEnum.Clear);
            for (var i = 2; i < Grids.PaveGrid.Cols.Count; ++i)
            {
                Grids.PaveGrid.Subtotal(AggregateEnum.Average, 0, 0, i, "Avgs");
            }

            CellStyle cs = Grids.PaveGrid.Styles[CellStyleEnum.Subtotal0];
            cs.BackColor = Color.Black;
            cs.ForeColor = Color.White;
            cs.Font = new Font(FontFamily.GenericSerif, 9, FontStyle.Bold);
            cs.TextAlign = TextAlignEnum.CenterCenter;
*/