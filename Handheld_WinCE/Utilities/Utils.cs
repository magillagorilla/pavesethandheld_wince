﻿

namespace Handheld_WinCE.Utilities
{
    public enum Tabs
    {
        Paving = 0, Dips = 1
    }

    public class GridIndex
    {
        public int Row { get { return rows[Tab]; } }
        public int Col { get { return cols[Tab]; } }
        public int Tab { get; set; }

        private readonly int[] rows = { 1, 1 };
        private readonly int[] cols = { 1, 1 };

        public GridIndex()
        {
            Tab = 0;
        }

        public void Update(int r, int c)
        {
            rows[Tab] = r;
            cols[Tab] = c;
        }
    }
}