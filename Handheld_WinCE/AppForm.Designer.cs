﻿namespace Handheld_WinCE
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu AppMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AppMenu = new System.Windows.Forms.MainMenu();
            this.MenuFile = new System.Windows.Forms.MenuItem();
            this.MenuFile_Save = new System.Windows.Forms.MenuItem();
            this.MenuFile_SavePaving = new System.Windows.Forms.MenuItem();
            this.MenuFile_SaveDips = new System.Windows.Forms.MenuItem();
            this.MenuFile_Open = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.MenuFile_Check = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.MenuFile_Help = new System.Windows.Forms.MenuItem();
            this.MenuFile_Exit = new System.Windows.Forms.MenuItem();
            this.MenuEdit = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Cut = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Copy = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Paste = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.MenuEdit_CopyDown = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.MenuEdit_Dipsheet = new System.Windows.Forms.MenuItem();
            this.MenuEdit_DipEmpty = new System.Windows.Forms.MenuItem();
            this.MenuEdit_DipPaving = new System.Windows.Forms.MenuItem();
            this.AppTabs = new System.Windows.Forms.TabControl();
            this.TabPaving = new System.Windows.Forms.TabPage();
            this.TabDipsheet = new System.Windows.Forms.TabPage();
            this.MenuEdit_CalcAvgs = new System.Windows.Forms.MenuItem();
            this.AppTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // AppMenu
            // 
            this.AppMenu.MenuItems.Add(this.MenuFile);
            this.AppMenu.MenuItems.Add(this.MenuEdit);
            // 
            // MenuFile
            // 
            this.MenuFile.MenuItems.Add(this.MenuFile_Save);
            this.MenuFile.MenuItems.Add(this.MenuFile_Open);
            this.MenuFile.MenuItems.Add(this.menuItem4);
            this.MenuFile.MenuItems.Add(this.MenuFile_Check);
            this.MenuFile.MenuItems.Add(this.menuItem6);
            this.MenuFile.MenuItems.Add(this.MenuFile_Help);
            this.MenuFile.MenuItems.Add(this.MenuFile_Exit);
            this.MenuFile.Text = "FILE";
            // 
            // MenuFile_Save
            // 
            this.MenuFile_Save.MenuItems.Add(this.MenuFile_SavePaving);
            this.MenuFile_Save.MenuItems.Add(this.MenuFile_SaveDips);
            this.MenuFile_Save.Text = "Save";
            // 
            // MenuFile_SavePaving
            // 
            this.MenuFile_SavePaving.Text = "Paving";
            this.MenuFile_SavePaving.Click += new System.EventHandler(this.MenuFile_SavePaving_Click);
            // 
            // MenuFile_SaveDips
            // 
            this.MenuFile_SaveDips.Text = "Dipsheet";
            this.MenuFile_SaveDips.Click += new System.EventHandler(this.MenuFile_SaveDips_Click);
            // 
            // MenuFile_Open
            // 
            this.MenuFile_Open.Text = "Open";
            // 
            // menuItem4
            // 
            this.menuItem4.Text = "-";
            // 
            // MenuFile_Check
            // 
            this.MenuFile_Check.Text = "Check File";
            this.MenuFile_Check.Click += new System.EventHandler(this.MenuFile_Check_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Text = "-";
            // 
            // MenuFile_Help
            // 
            this.MenuFile_Help.Text = "Help";
            // 
            // MenuFile_Exit
            // 
            this.MenuFile_Exit.Text = "Exit";
            // 
            // MenuEdit
            // 
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Cut);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Copy);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Paste);
            this.MenuEdit.MenuItems.Add(this.menuItem10);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_CopyDown);
            this.MenuEdit.MenuItems.Add(this.menuItem11);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_Dipsheet);
            this.MenuEdit.MenuItems.Add(this.MenuEdit_CalcAvgs);
            this.MenuEdit.Text = "EDIT";
            this.MenuEdit.Popup += new System.EventHandler(this.MenuEdit_Popup);
            // 
            // MenuEdit_Cut
            // 
            this.MenuEdit_Cut.Text = "Cut";
            // 
            // MenuEdit_Copy
            // 
            this.MenuEdit_Copy.Text = "Copy";
            // 
            // MenuEdit_Paste
            // 
            this.MenuEdit_Paste.Text = "Paste";
            // 
            // menuItem10
            // 
            this.menuItem10.Text = "-";
            // 
            // MenuEdit_CopyDown
            // 
            this.MenuEdit_CopyDown.Text = "&Copy Down";
            this.MenuEdit_CopyDown.Click += new System.EventHandler(this.MenuEdit_CopyDown_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Text = "-";
            // 
            // MenuEdit_Dipsheet
            // 
            this.MenuEdit_Dipsheet.MenuItems.Add(this.MenuEdit_DipEmpty);
            this.MenuEdit_Dipsheet.MenuItems.Add(this.MenuEdit_DipPaving);
            this.MenuEdit_Dipsheet.Text = "Create Dipsheet";
            // 
            // MenuEdit_DipEmpty
            // 
            this.MenuEdit_DipEmpty.Text = "Empty";
            this.MenuEdit_DipEmpty.Click += new System.EventHandler(this.MenuEdit_DipEmpty_Click);
            // 
            // MenuEdit_DipPaving
            // 
            this.MenuEdit_DipPaving.Text = "Paving";
            this.MenuEdit_DipPaving.Click += new System.EventHandler(this.MenuEdit_DipPaving_Click);
            // 
            // AppTabs
            // 
            this.AppTabs.Controls.Add(this.TabPaving);
            this.AppTabs.Controls.Add(this.TabDipsheet);
            this.AppTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AppTabs.Location = new System.Drawing.Point(0, 0);
            this.AppTabs.Name = "AppTabs";
            this.AppTabs.SelectedIndex = 0;
            this.AppTabs.Size = new System.Drawing.Size(240, 268);
            this.AppTabs.TabIndex = 0;
            this.AppTabs.SelectedIndexChanged += new System.EventHandler(this.AppTabs_SelectedIndexChanged);
            // 
            // TabPaving
            // 
            this.TabPaving.Location = new System.Drawing.Point(0, 0);
            this.TabPaving.Name = "TabPaving";
            this.TabPaving.Size = new System.Drawing.Size(240, 245);
            this.TabPaving.Text = "PAVING LEVELS    ";
            // 
            // TabDipsheet
            // 
            this.TabDipsheet.Location = new System.Drawing.Point(0, 0);
            this.TabDipsheet.Name = "TabDipsheet";
            this.TabDipsheet.Size = new System.Drawing.Size(240, 245);
            this.TabDipsheet.Text = "DIPSHEET    ";
            // 
            // MenuEdit_CalcAvgs
            // 
            this.MenuEdit_CalcAvgs.Text = "Calc Averages";
            this.MenuEdit_CalcAvgs.Click += new System.EventHandler(this.MenuEdit_CalcAvgs_Click);
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.AppTabs);
            this.KeyPreview = true;
            this.Menu = this.AppMenu;
            this.MinimizeBox = false;
            this.Name = "AppForm";
            this.Text = "PAVESET";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AppForm_KeyPress);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AppForm_KeyDown);
            this.AppTabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem MenuFile;
        private System.Windows.Forms.MenuItem MenuEdit;
        private System.Windows.Forms.TabControl AppTabs;
        private System.Windows.Forms.TabPage TabPaving;
        private System.Windows.Forms.TabPage TabDipsheet;
        private System.Windows.Forms.MenuItem MenuFile_Save;
        private System.Windows.Forms.MenuItem MenuFile_Open;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem MenuFile_Check;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem MenuFile_Help;
        private System.Windows.Forms.MenuItem MenuFile_Exit;
        private System.Windows.Forms.MenuItem MenuFile_SavePaving;
        private System.Windows.Forms.MenuItem MenuFile_SaveDips;
        private System.Windows.Forms.MenuItem MenuEdit_Cut;
        private System.Windows.Forms.MenuItem MenuEdit_Copy;
        private System.Windows.Forms.MenuItem MenuEdit_Paste;
        private System.Windows.Forms.MenuItem MenuEdit_CopyDown;
        private System.Windows.Forms.MenuItem MenuEdit_Dipsheet;
        private System.Windows.Forms.MenuItem MenuEdit_DipEmpty;
        private System.Windows.Forms.MenuItem MenuEdit_DipPaving;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem MenuEdit_CalcAvgs;
    }
}