﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Handheld_WinCE
{
    /// <summary>
    /// Class to hold paving levels & dip-sheet data & the necessary helper functions
    /// </summary>
    public class DataGridTables
    {
        /// <summary>
        /// Paving levels data table
        /// </summary>
        public DataTable Paving { get; private set; }

        /// <summary>
        /// Dip-Sheet data table
        /// </summary>
        public DataTable DipSheet { get; private set; }

        /// <summary>
        /// Default constructor -> initialise paving levels table
        /// </summary>
        public DataGridTables()
        {
            Paving = new DataTable("PavingTable");

            // create chainage column
            Paving.Columns.Add(new DataColumn("CH")
            {
                AllowDBNull = true,
                DataType = typeof(float),
                Caption = "CH",
                Unique = false
            });

            // create 30 identical columns for paving levels
            for (int i = 1; i <= 30; ++i)
            {
                var col = new DataColumn(i.ToString())
                {
                    AllowDBNull = true,
                    DataType = typeof(int),
                    Caption = i.ToString(),
                    Unique = false
                };

                Paving.Columns.Add(col);
            }
        }

        /// <summary>
        /// Creates the dip-sheet data table using the number of columns/rows present in the paving levels table
        /// </summary>
        /// <param name="rows">Number of used rows (chainages)</param>
        /// <param name="cols">Number of used columns (runs)</param>
        /// <param name="paving"></param>
        public void CreateDipSheetTable(int rows, int cols, bool paving)
        {
            DipSheet = new DataTable("DipSheet"); // create datatable object

            // create the chainage columm
            DipSheet.Columns.Add(new DataColumn("CH")
            {
                AllowDBNull = true,
                DataType = typeof(float),
                Caption = "CH",
                Unique = false,
                ReadOnly = false
            });

            // create the correct number of levels columns
            for (int i = 0; i < cols; ++i)
            {
                var col = new DataColumn(i.ToString())
                {
                    AllowDBNull = true,
                    DataType = typeof(int),
                    Caption = i.ToString(),
                    Unique = false
                };

                DipSheet.Columns.Add(col);
            }

            // create the required number of rows & copy in chainage values
            for (int i = 0; i < rows; ++i)
            {
                DipSheet.Rows.Add(DipSheet.NewRow());
                if (paving && Paving.Rows[i][0] != null)
                {
                    DipSheet.Rows[i][0] = Paving.Rows[i][0];
                }
            }

            //DipSheet.Columns[0].ReadOnly = true; // disable editing chainage values
        }

        /// <summary>
        /// The total number of data columns (chainage + levels)
        /// </summary>
        public int ColCount { get { return 31; } }


        /// <summary>
        /// Creates a new row with the correct columns & adds it to the bottom of the table
        /// </summary>
        /// <param name="count">The number of empty rows to add -> defaults to 1</param>
        /// <param name="tab"></param>
        public void AddRows(int count, int tab)
        {           
            var grid = tab == 0 ? Paving : DipSheet;
            var r = Enumerable.Repeat((object)null, grid.Columns.Count).ToArray();
            while (count-- > 0)
            {
                grid.Rows.Add(r);
            }
        }

        /// <summary>
        /// Removes all the rows of data from the paving & dip-sheet tables
        /// </summary>
        public void ClearRows()
        {
            if (Paving != null && Paving.Rows.Count > 0) { Paving.Rows.Clear(); }
            if (DipSheet != null && DipSheet.Rows.Count > 0) { DipSheet.Rows.Clear(); }
        }

        /// <summary>
        /// Fills the paving data table with values pulled from file (csv or pvs)
        /// </summary>
        /// <param name="data">2D list of cell values stored as strings</param>
        public void FillTable(List<List<string>> data)
        {
            foreach (List<string> row in data)
            {
                Paving.Rows.Add(row.Select(x => x as object).ToArray());
            }
        }
    }
}
